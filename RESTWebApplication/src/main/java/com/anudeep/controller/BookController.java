package com.anudeep.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.anudeep.dao.BookRepository;
import com.anudeep.model.Book;

@RestController
@CrossOrigin(origins="http://localhost:3000")
public class BookController {

	@Autowired
	BookRepository repository;
	
	@GetMapping("/books")
	public List<Book> getBooks() {
		return repository.findAll();
	}
	
	@GetMapping("/book/{id}")
	public Optional<Book> getBook(@PathVariable int id) {
		
		return repository.findById(id);
	}
	
	@PostMapping("/book")
	public Book addBook(@RequestBody Book book) {
		
		repository.save(book);
		return book;
	}
	
	@DeleteMapping("/book/{id}")
	public int deleteBook(@PathVariable int id) {
		
		Book book = repository.getOne(id);
		repository.delete(book);
		
		return id;
	}
	
	@PutMapping("/book")
	public Book updateBook(@RequestBody Book book) {
		
		repository.save(book);
		return book;
	}
}

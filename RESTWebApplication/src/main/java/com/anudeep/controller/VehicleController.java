package com.anudeep.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.anudeep.dao.VehicleRepository;
import com.anudeep.model.Vehicle;

@RestController
public class VehicleController {
	
	@Autowired
	VehicleRepository repository;
	
	@GetMapping("/vehicles")
	public List<Vehicle> getVehicles() {
		
		return repository.findAll();
	}
	
	@GetMapping("/vehicle/{id}")
	public Optional<Vehicle> getVehicle(@PathVariable int id) {
		
		return repository.findById(id);
	}
	
	@PostMapping("/vehicle")
	public Vehicle addVehicle(@RequestBody Vehicle vehicle) {
		
		repository.save(vehicle);
		return vehicle;
	}
	
	@DeleteMapping("/vehicle/{id}")
	public String deleteVehicle(@PathVariable int id) {
		
		Vehicle vehicle = repository.getOne(id);
		repository.delete(vehicle);
		
		return "Deleted vehicle with id = " + id + "  successfully!";
	}
	
	@PutMapping("/vehicle")
	public Vehicle updateVehicle(@RequestBody Vehicle vehicle) {
		
		repository.save(vehicle);
		return vehicle;
	}

}

package com.anudeep.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.anudeep.dao.StudentRepository;
import com.anudeep.model.Student;

@RestController
public class StudentController {
	
	@Autowired
	StudentRepository repository;
	
	@GetMapping("/students")
	public List<Student> getStudents() {
		
		return repository.findAll();
	}
	
	@GetMapping("/student/{id}")
	public Optional<Student> getStudent(@PathVariable int id) {
		
		return repository.findById(id);
	}
	
	@PostMapping("/student")
	public Student addStudent(@RequestBody Student student) {
		
		repository.save(student);
		return student;
	}
	
	@DeleteMapping("/student/{id}")
	public String deleteStudent(@PathVariable int id) {
		
		Student student = repository.getOne(id);
		repository.delete(student);
		
		return "Deleted student with id = " + id + "  successfully!";
	}
	
	@PutMapping("/student")
	public Student updateStudent(@RequestBody Student student) {
		
		repository.save(student);
		return student;
	}

}

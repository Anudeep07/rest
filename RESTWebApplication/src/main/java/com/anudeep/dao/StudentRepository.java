package com.anudeep.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anudeep.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>  {

}

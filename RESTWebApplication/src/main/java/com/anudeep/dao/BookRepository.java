package com.anudeep.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anudeep.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}

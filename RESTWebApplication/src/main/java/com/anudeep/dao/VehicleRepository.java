package com.anudeep.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anudeep.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {

}

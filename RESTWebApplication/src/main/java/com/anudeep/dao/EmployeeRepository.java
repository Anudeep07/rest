package com.anudeep.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anudeep.model.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}

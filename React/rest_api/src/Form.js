import React, { Component } from 'react';
import {Form,  FormGroup, Label, Input } from 'reactstrap';

class FormComponent extends Component {
    constructor(props) {
        super(props);

        this.initialState = {
            id: '',
            title: '',
            author: '',
            price: '',
            genre: ''
        };

        this.state = this.initialState;
    }

    handleChange = event => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        });
    }

    render() {
        const { title, author, price, genre } = this.state;

        return (
            <Form>
                <FormGroup>
                    <Label>Title</Label>
                    <Input
                        type="text"
                        name="title"
                        value={title}
                        onChange={this.handleChange}
                    />
                </FormGroup>


                <FormGroup>
                    <Label>Author</Label>
                    <Input
                        type="text"
                        name="author"
                        value={author}
                        onChange={this.handleChange}
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Price</Label>
                    <Input
                        type="number"
                        name="price"
                        value={price}
                        onChange={this.handleChange}
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Genre</Label>
                    <Input
                        type="text"
                        name="genre"
                        value={genre}
                        onChange={this.handleChange}
                    />
                </FormGroup>


                <FormGroup>
                    <Input
                        type="button"
                        value="Submit"
                        onClick={this.submitForm}
                    />
                </FormGroup>

            </Form>
        );
    }

    submitForm = () => {

        this.props.handleSubmit(this.state);
        this.setState(this.initialState);
    }
}

export default FormComponent;
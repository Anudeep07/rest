import React, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'reactstrap';
import FormComponent from './Form';

class App extends Component {

  state = {
    books: []
  }

  //get book (get)
  componentWillMount() {
    axios.get('http://localhost:8080/books')
      .then(response => {
        this.setState({
          books: response.data
        })
      })
      .catch(err => console.log(err));
  }

  //add book (post)
  handleSubmit = book => {

    axios.post('http://localhost:8080/book', book)
      .then(response => {
        book.id = response.data.id;

        this.setState({ books: [...this.state.books, book] });
      });
  }

  /*
  //update book (put)
  editBook = book => {
    axios.put('http://localhost:8080/book', book)
      .then(response => {
        console.log(response);
      });
  }
  */

  //delete book (delete)
  deleteBook = id => {
    axios.delete('http://localhost:8080/book/' + id)
      .then(response => {

        const { books } = this.state;

        this.setState({
          books: books.filter((book) => {
            return book.id !== id;
          })
        });
      });
  }

  render() {

    let books = this.state.books.map(book => {
      return (
        <tr key={book.id}>
          <td>{book.id}</td>
          <td>{book.title}</td>
          <td>{book.author}</td>
          <td>{book.price}</td>
          <td>{book.genre}</td>
          {/*           
          <td>
            <Button color="success" size="sm" onClick={this.editBook.bind(this, book)}>Edit</Button>*/}
          <td>
            <Button color="danger" size="sm" onClick={this.deleteBook.bind(this, book.id)}>Delete</Button>
          </td>

        </tr>
      )
    })

    return (

      <div className="App container">
        <Table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Price</th>
              <th>Genre</th>
              <th>Actions</th>
            </tr>
          </thead>

          <tbody>
            {books}
          </tbody>

        </Table>

        <FormComponent handleSubmit={this.handleSubmit} />
      </div>
    );
  }
}

export default App;
